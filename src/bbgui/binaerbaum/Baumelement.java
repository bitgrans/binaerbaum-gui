package bbgui.binaerbaum;

import bbgui.zeichnung.ZElement;

public abstract class Baumelement implements ZElement {

	public abstract Baumelement einfuegen(int zahl);

	public abstract Baumelement entfernen(int zahl);

	public abstract boolean istVorhanden(int zahl);

	public abstract int suchen(int zahl);

	public abstract void inorderAusgeben();

	public abstract void preorderAusgeben();

	public abstract void postorderAusgeben();

	public abstract void strukturAusgeben(int tiefe);

	public abstract int gibTiefe();

	public abstract int gibHoehe();
	
	protected abstract Baumelement rechtsEinfuegen(Baumelement element);

}
