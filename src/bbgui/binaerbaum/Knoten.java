package bbgui.binaerbaum;

import bbgui.zeichnung.ZElement;

public class Knoten extends Baumelement {

	private int zahl;
	private Baumelement linkerNachfolger;
	private Baumelement rechterNachfolger;

	public Knoten(int zahl) {
		this.zahl = zahl;
		linkerNachfolger = new Abschluss();
		rechterNachfolger = new Abschluss();
	}

	@Override
	public ZElement gibLinkerNachfolger() {
		return linkerNachfolger;
	}

	@Override
	public ZElement gibRechterNachfolger() {
		return rechterNachfolger;
	}

	@Override
	public String gibDaten() {
		return zahl + "";
	}

	@Override
	public boolean istSichtbar() {
		return true;
	}

	@Override
	public Baumelement einfuegen(int zahl) {
		if (this.zahl == zahl) {
			System.err.println("Zahl " + zahl + " ist bereits vorhanden!");
		} else if (this.zahl > zahl) {
			linkerNachfolger = linkerNachfolger.einfuegen(zahl);
		} else if (this.zahl < zahl) {
			rechterNachfolger = rechterNachfolger.einfuegen(zahl);
		}
		return this;
	}

	@Override
	public Baumelement entfernen(int zahl) {
		if (this.zahl > zahl) {
			linkerNachfolger = linkerNachfolger.entfernen(zahl);
			return this;
		} else if (this.zahl < zahl) {
			rechterNachfolger = rechterNachfolger.entfernen(zahl);
			return this;
		} else {
			linkerNachfolger = linkerNachfolger.rechtsEinfuegen(rechterNachfolger);
			return linkerNachfolger;
		}
	}

	@Override
	public boolean istVorhanden(int zahl) {
		if (this.zahl == zahl) {
			return true;
		} else {
			if (zahl > this.zahl) {
				return rechterNachfolger.istVorhanden(zahl);
			} else {
				return linkerNachfolger.istVorhanden(zahl);
			}
		}
	}

	@Override
	public int suchen(int zahl) {
		return 0;
	}

	@Override
	public void inorderAusgeben() {
		linkerNachfolger.inorderAusgeben();
		System.out.println(zahl);
		rechterNachfolger.inorderAusgeben();
	}

	@Override
	public void preorderAusgeben() {
		System.out.println(zahl);
		linkerNachfolger.preorderAusgeben();
		rechterNachfolger.preorderAusgeben();
	}

	@Override
	public void postorderAusgeben() {
		linkerNachfolger.postorderAusgeben();
		rechterNachfolger.postorderAusgeben();
		System.out.println(zahl);
	}

	@Override
	public void strukturAusgeben(int tiefe) {
		String leer = "";
		
		int i = 0;
		while (i < tiefe) {
			leer += "\t";
			i++;
		}
		
		System.out.println(leer + zahl);

		linkerNachfolger.strukturAusgeben(tiefe + 1);
		rechterNachfolger.strukturAusgeben(tiefe + 1);
	}

	@Override
	public int gibTiefe() {
		return 0;
	}

	@Override
	public int gibHoehe() {
		return Math.max(linkerNachfolger.gibHoehe(), rechterNachfolger.gibHoehe()) + 1;
	}

	@Override
	protected Baumelement rechtsEinfuegen(Baumelement element) {
		rechterNachfolger = rechterNachfolger.rechtsEinfuegen(element);
		return this;
	}

}
