package bbgui.binaerbaum;

import bbgui.zeichnung.ZElement;

public class Abschluss extends Baumelement {

	@Override
	public ZElement gibLinkerNachfolger() {
		return null;
	}

	@Override
	public ZElement gibRechterNachfolger() {
		return null;
	}

	@Override
	public String gibDaten() {
		return null;
	}

	@Override
	public boolean istSichtbar() {
		return false;
	}

	@Override
	public Baumelement einfuegen(int zahl) {
		return new Knoten(zahl);
	}

	@Override
	public Baumelement entfernen(int zahl) {
		return this;
	}

	@Override
	public boolean istVorhanden(int zahl) {
		return false;
	}

	@Override
	public int suchen(int zahl) {
		return 0;
	}

	@Override
	public void inorderAusgeben() {

	}

	@Override
	public void preorderAusgeben() {

	}

	@Override
	public void postorderAusgeben() {

	}

	@Override
	public void strukturAusgeben(int tiefe) {

	}

	@Override
	public int gibTiefe() {
		return 0;
	}

	@Override
	public int gibHoehe() {
		return 0;
	}

	@Override
	protected Baumelement rechtsEinfuegen(Baumelement element) {
		return element;
	}

}
