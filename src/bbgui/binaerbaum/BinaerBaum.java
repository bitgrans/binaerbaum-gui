package bbgui.binaerbaum;

import bbgui.zeichnung.ZBaum;
import bbgui.zeichnung.ZElement;

public class BinaerBaum implements ZBaum {
	
	private Baumelement wurzel;
	
	public BinaerBaum() {
		wurzel = new Abschluss();
	}	

	@Override
	public void einfuegen(int zahl) {
		wurzel = wurzel.einfuegen(zahl);
	}

	@Override
	public void entfernen(int zahl) {
		wurzel = wurzel.entfernen(zahl);
	}

	@Override
	public boolean istVorhanden(int zahl) {
		return wurzel.istVorhanden(zahl);
	}

	@Override
	public int suchen(int zahl) {
		return 0;
	}

	@Override
	public void inorderAusgeben() {
		wurzel.inorderAusgeben();
	}

	@Override
	public void preorderAusgeben() {
		wurzel.preorderAusgeben();
	}

	@Override
	public void postorderAusgeben() {
		wurzel.postorderAusgeben();
	}

	@Override
	public void strukturAusgeben() {
		wurzel.strukturAusgeben(0);
	}

	@Override
	public int gibTiefe() {
		return gibHoehe() - 1;
	}

	@Override
	public int gibHoehe() {
		return wurzel.gibHoehe();
	}

	@Override
	public ZElement gibWurzel() {
		return wurzel;
	}

}
