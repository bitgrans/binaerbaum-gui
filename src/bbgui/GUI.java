package bbgui;

import bbgui.zeichnung.BaumZeichnung;
import bbgui.zeichnung.ZBaum;
import jGUItoolbox.D_Meldung;
import jGUItoolbox.Eingabefeld;
import jGUItoolbox.ITuWas;
import jGUItoolbox.Taste;
import jGUItoolbox.Zeichnung;

public class GUI implements ITuWas {

	private BaumZeichnung zeichnung;
	private ZBaum baum;

	private Eingabefeld zahleingabe;

	private int tastenhoehe = 30;
	private int tastenbreite = 200;

	private int ypos = 1;

	public GUI(BaumZeichnung zeichnung, ZBaum baum) {
		this.zeichnung = zeichnung;
		this.baum = baum;

		Zeichnung.gibJFrame().setTitle("Binärbaum Steuerung");

		zahleingabe = new Eingabefeld("", 0, ypos, tastenbreite, tastenhoehe);

		tasteEinfuegen("Zahl einfügen", 1);
		tasteEinfuegen("Zahl entfernen", 2);
		tasteEinfuegen("Ist vorhanden", 3);
		tasteEinfuegen("Suchen", 4);
		tasteEinfuegen("Inorder", 5);
		tasteEinfuegen("Preorder", 6);
		tasteEinfuegen("Postorder", 7);
		tasteEinfuegen("Struktur", 8);
		tasteEinfuegen("Tiefe", 9);
		tasteEinfuegen("Hoehe", 10);
	}

	private Taste tasteEinfuegen(String name, int id) {
		Taste t = new Taste(name, 0, ypos * tastenhoehe, tastenbreite, tastenhoehe);
		t.setzeSchriftgroesse(15);
		t.setzeLink(this, id);
		ypos++;
		return t;
	}

	public double gibHoehe() {
		return (ypos + 1) * tastenhoehe;
	}

	public double gibBreite() {
		return tastenbreite;
	}

	@Override
	public void tuWas(int c) {
		String eingabe = zahleingabe.leseText();
		int zahl = 0;
		try {
			zahl = Integer.parseInt(eingabe);
		} catch (NumberFormatException e) {
			// System.err.println("Eingabe ist keine Zahl!");
			e.printStackTrace();
			return;
		}

		D_Meldung m = new D_Meldung();

		switch (c) {
		case 1:
			baum.einfuegen(zahl);
			break;

		case 2:
			baum.entfernen(zahl);
			break;

		case 3:
			m.setzeTitel("Methode: istVorhanden()");
			if (baum.istVorhanden(zahl)) {
				m.setzeMeldungstext(zahl + " ist vorhanden.");
			} else {
				m.setzeMeldungstext(zahl + " ist nicht vorhanden.");
			}
			m.zeigeMeldung();
			break;

		case 4:
			// SINNLOS
			break;

		case 5:
			baum.inorderAusgeben();
			break;

		case 6:
			baum.preorderAusgeben();
			break;

		case 7:
			baum.postorderAusgeben();
			break;

		case 8:
			baum.strukturAusgeben();
			break;

		case 9:
			m.setzeTitel("Methode: gibTiefe()");
			m.setzeMeldungstext("Tiefe: " + baum.gibTiefe());
			m.zeigeMeldung();
			break;

		case 10:
			m.setzeTitel("Methode: gibHoehe()");
			m.setzeMeldungstext("Hoehe: " + baum.gibHoehe());
			m.zeigeMeldung();
			break;
		default:
			break;
		}
		zeichnung.zeichne();
	}

}
