package bbgui;

import bbgui.binaerbaum.BinaerBaum;
import bbgui.zeichnung.BaumZeichnung;
import jGUItoolbox.Zeichnung;
import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage stage) throws Exception {
		BinaerBaum b = new BinaerBaum();
		BaumZeichnung bz = new BaumZeichnung(b);
		GUI g = new GUI(bz, b);
		Zeichnung.gibJFrame().setVisible(false);

		SwingNode sn = new SwingNode();
		sn.setContent(Zeichnung.gibZeichenflaeche());

		stage.setMinWidth(g.gibBreite());
		stage.setMinHeight(g.gibHoehe());

		BorderPane rootLayout = new BorderPane();
		rootLayout.setLeft(sn);

		Pane p = new Pane();
		rootLayout.setCenter(p);
		p.getChildren().add(bz);
		bz.widthProperty().bind(p.widthProperty());
		bz.heightProperty().bind(p.heightProperty());

		Scene root = new Scene(rootLayout);
		root.addEventHandler(KeyEvent.KEY_PRESSED, key -> {
			if (key.getCode() == KeyCode.D) {
				bz.swDebug();
			} else if (key.getCode() == KeyCode.G) {
				bz.swHintergrund();
			}
		});

		stage.setScene(root);

		stage.setOnCloseRequest(event -> {
			System.exit(0);
		});
		stage.setTitle("BinaerBaum-GUI");
		stage.setWidth(600);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
