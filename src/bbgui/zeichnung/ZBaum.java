package bbgui.zeichnung;

public interface ZBaum {

	public void einfuegen(int zahl);

	public void entfernen(int zahl);

	public boolean istVorhanden(int zahl);

	public int suchen(int zahl);

	public void inorderAusgeben();

	public void preorderAusgeben();

	public void postorderAusgeben();

	public void strukturAusgeben();

	public int gibTiefe();

	public int gibHoehe();

	public ZElement gibWurzel();

}
