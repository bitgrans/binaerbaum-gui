package bbgui.zeichnung;

public interface ZElement {
	
	public ZElement gibLinkerNachfolger();
	
	public ZElement gibRechterNachfolger();
	
	public String gibDaten();
	
	public boolean istSichtbar(); // Abschluss soll nicht gezeichnet werden!

}
