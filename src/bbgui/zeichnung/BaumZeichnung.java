package bbgui.zeichnung;

import com.sun.javafx.tk.FontMetrics;
import com.sun.javafx.tk.Toolkit;

import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class BaumZeichnung extends Canvas {

	private ZBaum baum;

	private GraphicsContext gc;

	private boolean debug = false;
	private boolean hintergrund = false;

	private final Image garten;

	/**
	 * Initialisiert die Zeichenfläche.
	 * @param baum Referenz auf zeichenbaren Baum
	 */
	public BaumZeichnung(ZBaum baum) {
		this.baum = baum;
		gc = getGraphicsContext2D();

		garten = new Image(getClass().getResourceAsStream("/img/garten.jpg"));

		widthProperty().addListener(cl -> zeichne());
		heightProperty().addListener(cl -> zeichne());
	}

	/**
	 *  Klasse für Zeichnung einer Kante des Baums
	 */
	private static class Kante {
		private static final Color FARBE = Color.BLACK;
		private static final double BREITE = 2.0;

		private Point2D p1;
		private Point2D p2;

		public Kante(Point2D p1, Point2D p2) {
			this.p1 = p1;
			this.p2 = p2;
		}

		public void zeichne(GraphicsContext gc) {
			gc.setLineDashes(0);
			gc.setStroke(FARBE);
			gc.setLineWidth(BREITE);
			gc.strokeLine(p1.getX(), p1.getY(), p2.getX(), p2.getY());
		}
	}

	/**
	 * Klasse für Zeichnung eines Knotens des Baums
	 */
	private static class Knoten {
		private static final double RADIUS = 30.0;
		private static final double DURCHMESSER = 2 * RADIUS;
		private static final Color FARBE = Color.BLUE;
		private final FontMetrics fm = Toolkit.getToolkit().getFontLoader().getFontMetrics(Font.getDefault());
		private static final Color TEXTFARBE = Color.BLACK;
		private static final double KREISBREITE = 2.0;

		private double x;
		private double y;
		private String text;

		public Knoten(Point2D punkt, String text) {
			x = punkt.getX() - RADIUS;
			y = punkt.getY() - RADIUS;
			this.text = text;
		}

		public void zeichne(GraphicsContext gc) {
			gc.setLineDashes(0);
			gc.setFill(Color.WHITE);
			gc.fillOval(x, y, DURCHMESSER, DURCHMESSER);
			gc.setStroke(FARBE);
			gc.setLineWidth(KREISBREITE);
			gc.strokeOval(x, y, DURCHMESSER, DURCHMESSER);

			gc.setFill(TEXTFARBE);
			gc.fillText(text, (x + RADIUS) - fm.computeStringWidth(text) / 2, (y + RADIUS) + fm.getAscent() / 4);
		}
	}

	/**
	 * Zeichnet den kompletten Baum
	 */
	public void zeichne() {
		double w = getWidth();
		double h = getHeight();

		gc.clearRect(0, 0, w, h);

		if (hintergrund) {
			gc.drawImage(garten, 0, 0, w, h);
		}

		int baumHoehe = baum.gibHoehe();
		if (baumHoehe == 0) {
			return;
		}

		ZElement wurzel = baum.gibWurzel();
		zeichneKanten(wurzel, 0, w, 0, h / baumHoehe);
		zeichneKnoten(wurzel, 0, w, 0, h / baumHoehe);
	}

	/**
	 * Zeichnet die Kante eines Baums
	 * @param element Zu zeichnendes Baumelement
	 * @param x1 Rechteck Koordinate x1
	 * @param x2 Rechteck Koordinate x2
	 * @param y1 Rechteck Koordinate y1
	 * @param y2 Rechteck Koordinate y2
	 */
	private void zeichneKanten(ZElement element, double x1, double x2, double y1, double y2) {
		ZElement linkerNachfolger = element.gibLinkerNachfolger();
		ZElement rechterNachfolger = element.gibRechterNachfolger();

		Point2D p1 = new Point2D((x1 + x2) / 2, y1 + y2 / 2);

		if (linkerNachfolger.istSichtbar()) {
			Point2D p2 = new Point2D(((x1 + (x1 + x2) / 2) / 2), y1 + y2 + y2 / 2);
			Kante k = new Kante(p1, p2);
			k.zeichne(gc);

			zeichneKanten(linkerNachfolger, x1, (x1 + x2) / 2, y1 + y2, y2);
		}

		if (rechterNachfolger.istSichtbar()) {
			Point2D p2 = new Point2D((x2 + (x1 + x2) / 2) / 2, y1 + y2 + y2 / 2);
			Kante k = new Kante(p1, p2);
			k.zeichne(gc);

			zeichneKanten(rechterNachfolger, (x1 + x2) / 2, x2, y1 + y2, y2);
		}
	}

	/**
	 * Zeichnet die Knoten eines Baums
	 * @param element Zu zeichnendes Baumelement
	 * @param x1 Rechteck Koordinate x1
	 * @param x2 Rechteck Koordinate x2
	 * @param y1 Rechteck Koordinate y1
	 * @param y2 Rechteck Koordinate y2
	 */
	private void zeichneKnoten(ZElement element, double x1, double x2, double y1, double y2) {
		Point2D mittelpunkt = new Point2D((x1 + x2) / 2, (y1 + y2) / 2);

		if (element.istSichtbar()) {
			if (debug) {
				System.out.println("X1: " + x1 + ", X2: " + x2 + ", Y1: " + y1 + ", Y2: " + y2);

				gc.setLineDashes(40);
				gc.setStroke(Color.AQUA);
				gc.strokeRoundRect(x1, y1, x2, y2, 50, 50);
				gc.setLineDashes(20);
				gc.setStroke(Color.BROWN);
				gc.strokeLine(x1, (y1 + y2) / 2, x2, (y1 + y2) / 2);
				gc.strokeLine((x1 + x2) / 2, y2, (x1 + x2) / 2, y1);
			}

			Knoten k = new Knoten(mittelpunkt, element.gibDaten());
			k.zeichne(gc);

			zeichneKnoten(element.gibLinkerNachfolger(), x1, (x1 + x2) / 2, y2, y2 + (y2 - y1));
			zeichneKnoten(element.gibRechterNachfolger(), (x1 + x2) / 2, x2, y2, y2 + (y2 - y1));
		}
	}

	/**
	 * Schaltet die erweiterte Ausgabe ein/aus
	 */
	public void swDebug() {
		debug = !debug;
		zeichne();
	}

	/**
	 * Schaltet das Hintergrundbild ein/aus
	 */
	public void swHintergrund() {
		hintergrund = !hintergrund;
		zeichne();
	}
}
